import {Component, OnInit} from '@angular/core';
import {HttpService} from "./services/http-service";
import {HttpClient} from "@angular/common/http";
import {OfferModel} from "./models/offer.model";
import {filter, map} from "rxjs/operators";
import {Observable} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  offersList$: Observable<OfferModel[]>;


  constructor(private http: HttpService) {
  }

  ngOnInit(): void {
    this.offersList$ = this.http.getOffers();
  }

  title = 'order-entry';


  getDatafromChild($event: string) {
    console.log($event);
  }
}
