import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-new-component',
  templateUrl: './new-component.component.html',
  styleUrls: ['./new-component.component.css']
})
export class NewComponentComponent implements OnInit, OnDestroy {

  @Input()
  values: any;

  @Output()
  outputChild = new EventEmitter<string>();


  constructor() {

  }

  ngOnInit() {
    console.log(this.values);
  }

  ngOnDestroy(): void {

  }


  onButtonClick($event: MouseEvent) {
    this.outputChild.emit("output");
    console.log($event);
  }
}
